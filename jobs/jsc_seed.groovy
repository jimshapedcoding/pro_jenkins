def job = job('jsc_seed_job') {
    displayName('Seed Job')
    wrappers {
        preBuildCleanup()
    }
    parameters {
        stringParam('JOBS_REPOSITORY', 'https://gitlab.com/jimshapedcoding/pro_jenkins.git', 'Jobs Repository to clone')
        stringParam('JOBS_REPOSITORY_BRANCH', 'main', 'Name of the branch from jobs repository to clone')
    }
    scm {
        git {
            branch('${JOBS_REPOSITORY_BRANCH}')
            remote {
                name('origin')
                url('${JOBS_REPOSITORY}')
            }
        }
    }
    steps {
        dsl {
            // all *.groovy files in jobs directory will be processed
            external('jobs/*.groovy')
            additionalClasspath(['src/main/groovy', ].join('\n'))
            removeAction('IGNORE')
        }
        // You may also want to process your views:
        /*
        dsl {
            // all *.groovy files in views directory will be processed
            external('views/*.groovy')
            additionalClasspath(['src/main/groovy', ].join('\n'))
            removeViewAction('IGNORE')
        }
        */
    }
}
// This accepts changes in the script approval section of 'Manage Jenkins'.  You may or may not want this.
org.jenkinsci.plugins.scriptsecurity.scripts.ScriptApproval.get().with { approval ->
    approval.preapproveAll()
    approval.save()
}