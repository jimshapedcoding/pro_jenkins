def job = pipelineJob('jsc_abstractor') {
    displayName('JSC Abstractor Job')
    definition {
        cpsScm {
            lightweight(lightweight = false)
            scm {
                git {
                    remote {
                        url('${JOBS_REPOSITORY}')
                    }
                    extensions {
                        wipeOutWorkspace()
                    }
                    branch('${JOBS_REPOSITORY_BRANCH}')
                }
            }
            scriptPath('jobs/pipelines/jsc_abstractor/Jenkinsfile')
        }
    }
    parameters {
        stringParam('JOBS_REPOSITORY', 'https://gitlab.com/jimshapedcoding/pro_jenkins.git', 'Jobs Repository to clone')
        stringParam('JOBS_REPOSITORY_BRANCH', 'main', 'Name of the branch from jobs repository to clone')
        stringParam('foo', 'bar', 'Something to bar')
    }
}