import net.sf.json.JSONObject

def jsonEditorOptions = JSONObject.fromObject(/{
  "disable_edit_json": true,
  "disable_properties": true,
  "no_additional_properties": true,
  "disable_collapse": true,
  "disable_array_add": false,
  "disable_array_delete": false,
  "disable_array_reorder": false,
  "theme": "bootstrap3",
  "iconlib":"fontawesome5",
  "keep_oneof_values": true,
  "object_layout": "normal",
  "template": "default",
  "show_errors": "interaction",
  "prompt_before_delete": true,
  "schema": {
    "type": "array",
    "format": "tabs",
    "title": "Clusters Configuration",
    "uniqueItems": true,
    "items": {
      "type": "object",
      "title": "Cluster",
      "required":["CLUSTER_NAME"],
      "properties": {
        "CLUSTER_NAME": {
          "type": "string",
          "description": "Full cluster name including any prefix, which identifies the cluster."
        }
      }
    },
    "default": [{}, {}, {}]
  }
}/);
