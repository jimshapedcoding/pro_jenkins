def jobsConfiguration = [
    [
        'name': "jsc_multicluster",
        'displayName': "JSC Multi Cluster Job",
        'disableRebuild': false,
    ]
]

def scriptTemplate = readFileFromWorkspace("jobs/extended_choice_parameter/jsc_multicluster_configuration.groovy")
def templateEngine = new groovy.text.SimpleTemplateEngine()
def binding = [
     OCPVersions: listToJson([""] + ["4.1", "4.2"])
]
def clustersConfigurationParameterScript = templateEngine.createTemplate(scriptTemplate).make(binding)


for (jobConfig in jobsConfiguration) {
    def job = pipelineJob(jobConfig['name']) {
        displayName(jobConfig['displayName'])
        properties {
            rebuild {
                rebuildDisabled(jobConfig['disableRebuild'])
            }
        }
        definition {
            cpsScm {
                lightweight(lightweight = false)
                scm {
                    git {
                        remote {
                            url('${JOBS_REPOSITORY}')
                        }
                        extensions {
                            wipeOutWorkspace()
                        }
                        branch('${JOBS_REPOSITORY_BRANCH}')
                    }
                }
                scriptPath('jobs/pipelines/jsc_multicluster/Jenkinsfile')
            }
        }

        parameters {
            stringParam('JOBS_REPOSITORY', 'https://gitlab.com/jimshapedcoding/pro_jenkins.git', 'Jobs Repository to clone')
            stringParam('JOBS_REPOSITORY_BRANCH', 'main', 'Name of the branch from jobs repository to clone')
            stringParam('foo', 'bar', 'Something to bar')
            extendedChoice {
                name("CLUSTERS_CONFIGURATION")
                type("PT_JSON")
                groovyScript(clustersConfigurationParameterScript.toString())
                groovyClasspath("")
                saveJSONParameterToFile(false)
                groovyScriptFile("")
                value("")
                projectName("")
                propertyFile("")
                bindings("")
                propertyKey("")
                defaultValue("")
                defaultPropertyFile("")
                defaultGroovyScript("")
                defaultGroovyScriptFile("")
                defaultBindings("")
                defaultGroovyClasspath("")
                defaultPropertyKey("")
                descriptionPropertyValue("")
                descriptionPropertyFile("")
                descriptionGroovyScript("")
                descriptionGroovyScriptFile("")
                descriptionBindings("")
                descriptionGroovyClasspath("")
                descriptionPropertyKey("")
                javascriptFile("")
                javascript("")
                quoteValue(true)
                visibleItemCount(5)
                multiSelectDelimiter("")
            }
        }
        environmentVariables {
            keepSystemVariables(true)
            keepBuildVariables(true)
        }
    }
}

// TODO: Move to a common place ? Used both in this file and also in jobs/qe_multicluster.groovy
def listToJson(list) {
    // Return json representation of given list, additionally escape any slash (`/`)
    // with four backslashes - to get it properly propagated (`\\\\/`)
    return groovy.json.JsonOutput.toJson(list).replace("/", "\\\\/")
}